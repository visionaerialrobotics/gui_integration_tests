###  Graphical Teleoperation Integration Test

This test verifies the correct integrated operation of the  teleoperation control window  and the following componentes: the first person viewer, the environment viewer, the execution viewer and the vehicle dynamics viewer.

In order to execute this test, perform the following steps:

1.  Launch Aerostack with the graphical user interface (option 'View' > 'Launch Aerostack'>Select 'launcher_simulated_quadrotor_basic_3.0' file ).
2.  Select the following option: 'View' > 'Prefixed Layouts' > 'Graphical Teleoperation'.
3.  You can launch RViz simulator to watch a graphical representation of the drone flying using the following command:

         $ bash $AEROSTACK_STACK/launchers/rvizInterface_Test.sh

As answer to this, five windows (six if RViz simulator was launched) are displayed as it is shown in the next image: 'Teleoperation Control Window', 'First Person Viewer', 'Environment Viewer', 'Execution Viewer' and 'Vehicle Dynamics Viewer'.

![GraphicalTeleoperationControl.png](https://bitbucket.org/repo/rokr9B/images/734389812-GraphicalTeleoperationControl.png)
Perform the following actions to reproduce the test mission:

 1. Make sure that 'Graphical Teleoperation' window is selected.
 2. Click on 'Start' at the bottom of the window. 
 3. Press 'right arrow' to move frontwards and then press 'h' when you want to stop. 
 4. Press 'z' to turn the direction counter-clockwise until reaching contrary sense when you have to press 'h'. 
 5. Press 'left arrow' to move backwards and press 'h'  when arriving to the initial point.
 6. Click on 'Land' in the bottom of the Control Panel.

Here there is a video that shows the correct execution of the test:

[ ![Graphical Teleoperation](https://img.youtube.com/vi/wtuR5UKlkkc/0.jpg)](https://www.youtube.com/watch?v=wtuR5UKlkkc)
