##  Behavior Tree Integration Test

This test verifies the correct integrated operation of GUI windows corresponding to the behavior tree.

In order to execute this test, perform the following steps:

- Launch the graphical user interface of Aerostack.
- Select the option: 'Edit' > 'Mission Behavior Tree'.
- In the behavior tree editor, press 'New' button to create a new behavior tree. 
- Write the name of the mission in the dialog window: "Go to destination and come back".

![CreateNewBehaviorTree.png](https://bitbucket.org/repo/rokr9B/images/3124300420-CreateNewBehaviorTree.png)

- Right-click on node and select the option 'Add child'. 

![AddChild.png](https://bitbucket.org/repo/rokr9B/images/303913180-AddChild.png)

- Create the following steps of the mission (see here how to [edit a behavior tree](https://github.com/Vision4UAV/Aerostack/wiki/Behavior-Tree)):

1. Take off
2. Save the current position as home base (using variables X, Y and Z)
3. Go to the point A (1,14,1)
4. Go to the point B (1,12,1)
5. Go to home base (using variables X, Y and Z)
6. Land

![SaveBehaviorTree.png](https://bitbucket.org/repo/rokr9B/images/1647081092-SaveBehaviorTree.png)

- Click 'Accept' to exit from the edition mode. The behavior tree mission file with the name 'behavior_tree_mission_file.yaml' is saved in your configuration folder. 
- In the top menu bar of the GUI, select the option: 'Run' > 'Launch Aerostack', and wait until Aerostack is launched.
- In the top menu bar of the GUI, select the option: 'View' > 'Prefixed Layouts' > 'Behavior Tree Mission'.
- To execute the mission, press the 'Start mission' button at the bottom of the 'Behavior Tree Control' window. 

Here there is a video showing the complete process:

[ ![Behavior Tree Mission](https://img.youtube.com/vi/HZ5r878KtK8/0.jpg)](https://www.youtube.com/watch?v=HZ5r878KtK8)



