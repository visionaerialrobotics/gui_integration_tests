
##  Python Mission Integration Test

This test verifies the correct integrated operation of the  Python mission control window  and the following components:  the environment viewer, the execution viewer, the vehicle dynamics viewer and the Python based mission interpreter.

In order to execute this test, perform the following steps:

- Create a Python file with the following code named mission.py and save it in the folder `$AEROSTACK_STACK/configs/drone1`:

   
```python
import executive_engine_api as api

def runMission():
  print("Starting mission...")
  print("Taking off...")
  api.executeBehavior('TAKE_OFF')
  endPoint = [8, 8, 0.7]
  print("Going to landing point...")
  api.executeBehavior('GO_TO_POINT', coordinates=endPoint)
  print("Landing...")
  api.executeBehavior('LAND')

```
- Launch Aerostack with the graphical user interface (option 'View' > 'Launch Aerostack'>Select 'launcher_simulated_quadrotor_basic_3.0' file ).
- Select the following option: 'View' > 'Prefixed Layouts' > 'Python Mission'. As an answer to this, five windows are displayed as it is shown in the next image: 'Python Mission Control Window', 'Python Based Mission Interpreter', 'Environment Viewer', 'Execution Viewer' and 'Vehicle Dynamics Viewer'.

![GUIControlPanelStartMission.png](https://bitbucket.org/repo/rokr9B/images/85780605-GUIControlPanelStartMission.png)

- Press the `Start mission` at the bottom of the 'Python Mission Contol' window to reproduce the test mission. 

Here there is a video that shows the correct execution of the test:

[ ![Python Mission](https://img.youtube.com/vi/_F-vejjX7F0/0.jpg)](https://www.youtube.com/watch?v=_F-vejjX7F0)
