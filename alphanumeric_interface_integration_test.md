##  Alphanumeric Interface Integration Test

This test verifies the correct integrated operation of the [alphanumeric interface](https://github.com/Vision4UAV/Aerostack/wiki/Alphanumeric-User-Interface) and the environment viewer of the graphical user interface.

In order to execute this test, perform the following steps:

1. Launch Aerostack with the graphical user interface (option 'View' > 'Launch Aerostack').
1. Select the following option: 'View' > 'Prefixed Layouts' > 'Alphanumeric Interface'.

As answer to this, two windows are displayed as it is shown in the next image with a terminal window and the 'Environment Map Viewer'.

![Captura de pantalla de 2018-06-01 13-28-08.png](https://bitbucket.org/repo/rokr9B/images/4120630382-Captura%20de%20pantalla%20de%202018-06-01%2013-28-08.png)

Perform the following actions to reproduce the test mission:

 1. Press 't' to take off. You will see how z coordinate increases.  
 2. Press 'q' to increase altitude if necessary (press 'h' to stop increasing alitude).
 3. Press 'up arrow' to move frontwards.
 4. Press 'h' to stop the drone and keep it hovering.
 5. Press 'z' to turn the direction counter-clockwise until reaching contrary sense.
 6. Press 'h' to stop turning around.
 7. Press 'up arrow' to move frontwards in the new sense.
 8. Press 'h' to stop moving when arriving at initial point.
 9. Press 'y' to land.

Here there is a video that shows the correct execution of the test.

[ ![Alphanumeric Interface](https://img.youtube.com/vi/LRnajdIjre0/0.jpg)](https://www.youtube.com/watch?v=LRnajdIjre0)

