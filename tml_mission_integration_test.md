
##  TML Mission Integration Test

This test verifies the correct integrated operation of the  TML mission control window  and the following components:  the environment viewer, the execution viewer, the vehicle dynamics viewer and the task based mission interpreter.

In order to execute this test, perform the following steps:

- Create a XML file with the following code named mission_specification_file.xml  and save it in the folder `$AEROSTACK_STACK/configs/drone1`:
   
```xml
<mission root_task="mission">
  <task_tree>
    <task name= "mission">
      <task name="Take off">
        <behavior name="TAKE_OFF"/>
      </task>
      <task name="Moving to point A">
        <behavior name="GO_TO_POINT">
          <argument name="coordinates" value="[2, 3, 1]"/>
        </behavior>
      </task>
      <task name="Moving to point B">
        <behavior name="GO_TO_POINT">
          <argument name="coordinates" value="[1, 1, 0.5]"/>
        </behavior>
      </task>
      <task name="Moving to point C">
        <behavior name="GO_TO_POINT">
          <argument name="coordinates" value="[4, 4, 0.7]"/>
        </behavior>
      </task>
      <task name="Final land">
        <behavior name="LAND" />
      </task>
    </task>
  </task_tree>
</mission>

```
- Launch Aerostack with the graphical user interface (option 'View' > 'Launch Aerostack'>Select 'launcher_simulated_quadrotor_basic_3.0' file ).
- Select the following option: 'View' > 'Prefixed Layouts' > 'TML  Mission'. As an answer to this, five windows are displayed as it is shown in the next image: 'TML Mission Control Window', 'Task Based Mission Interpreter', 'Environment Viewer', 'Execution Viewer' and 'Vehicle Dynamics Viewer'.

![TmlOperationMode.png](https://bitbucket.org/repo/rokr9B/images/555066988-TmlOperationMode.png)

-  Press the `Start mission` at the bottom of the 'TML Mission Contol' window to reproduce the test mission.

Here there is a video that shows the correct execution of the test:

[ ![TML Mission](https://img.youtube.com/vi/zayHUotJh4w/0.jpg)](https://www.youtube.com/watch?v=zayHUotJh4w)